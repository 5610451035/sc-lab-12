package phone;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PhoneBook {
	
	private String file;
	private FileReader inReader;
	
	PhoneBook(String file){
		this.file = file;
	}
	
	public void readPhoneBook(){
		try {
			System.out.println("---Name Phone---");
			inReader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(inReader);
			int sum = 1;
			for (String line = buffer.readLine(); line != null; line = buffer.readLine()) {
				System.out.println(sum+" ---> " + line);
				sum++;
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file : "+file);
		}
		catch (IOException e) {
			System.err.println("Error reading from file");
		}
	}


}
