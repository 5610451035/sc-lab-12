package countword;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class WordCounter {
	
	String file;
	HashMap<String,Integer> wordCount;
	private FileReader inReader;
	
	WordCounter(String file){
		this.file = file;
		wordCount = new HashMap<String,Integer>();
	}

	public void count(){
		try{
			inReader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(inReader);
			for (String line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] words = line.split(" ");
				for (int i = 0; i<words.length; i++) {
		            if (wordCount.containsKey(words[i]) == false) {
		            	wordCount.put(words[i], 1);
		            } 
		            else {
		            	wordCount.replace(words[i], wordCount.get(words[i]), wordCount.get(words[i])+1);
		            }
		        }
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+file);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (inReader != null)
					inReader.close();
			} 
			catch (IOException e) {
				System.err.println("Error closing files");
			}
		}	
		}
		
		
	public String hasWord() {
		String str = "";
		for (String key : wordCount.keySet()) {
			str = str + key + " := " + wordCount.get(key) + "\n";
		}
		return str;
	}
	
	
}